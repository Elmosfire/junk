System: Mutants and masterminds

+3 in every ability score.

Every one gets these powers:

Omniscience: 
 - You can ask the dm any question (yes/no questions or open questions) that the dm will answer truthfully. The dm is free in how to interpret the question, and can ask the player to reformulate. The dm can only answer questions that are objectivite, and that apply to the current plane of existance. Questions must be asked from the perspective of the charcter, metagaming is not allowed. If a question is argumentative the dm will state so, and allow the player to rephrase the question. 
 - Usable once per player per 30 ingame minutes.
 - details: custom
 - Action: Standard • Range: n/a • Duration: Instant

Destiny bound: 
 - If one player dies, all players die. (no effect on other damage conditions).
 - details: custom
 - Action: n/a • Range: n/a • Duration: Permanent

And each player can pick 1 of these powers:

Puppet: 
 - The enemy makes a DC 20 fortitude check, if the target fails, the target is fully under the control of the caster. These form of mind control does not give th player access to the memories or personality of the caster, only the body.
 - details: Affliction rank 20: 30 DC (none/none/controlled) fortitude.
 - Action: Standard • Range: Perception (vision) • Duration: Continuous

Creation: 
 -  You can form solid brass-metal objects essentially out of nowhere. You can form any simple geometric shape or common object (such as a cube, sphere, dome, hammer, lens, disk, etc.). A complex mechanical object, like a gearbox or a gun is allowed, and an object where you need mechanical information that you do not have is also fine (For example, you can create "a key that fits the lock I am pointing at.). It is not allowed to make a piece of electronics, nor an object that transfers information to you (like a piece of paper with a keycode on it, if you do not know the keycode).
 - details: Create rank 5 (30 cft.) +Continuous, Movable, Precise, Stationary, Subtle(1), Tether
 - Action: Standard • Range: Close • Duration: Continuous

Banshee wail:
 - All enemies that can hear you makes a DC 30 will save, or dies
 - details: Affliction rank 30: 40 DC (none/none/death) will.
 - Action: Standard • Range: area perception (sound) • Duration: Instant

Echo:
 - You can create up to 5 duplicates of yourself, that are mind linked. You can dismiss any duplicate (or the original) at will. Destiny bound also applies to duplicates. 
 - details: Duplication, Multiple Minions(3), Mental Link, Heroic, 
 - Action: Standard • Range: Close • Duration: Continuous





